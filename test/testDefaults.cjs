const testObject = require("../objects.cjs");
const getObjectDetails = require("../defaults.cjs");
const defaultProps = {
  name: "Bruce Wayne",
  age: 36,
  role: "Hero",
  location: "Gotham",
  alias: "Batman",
};

const result = getObjectDetails(testObject, defaultProps);
console.log(result);
