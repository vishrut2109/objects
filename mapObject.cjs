function mapObject(testObject, callBac) {
  if (testObject) {
    for (const index in testObject) {
      testObject[index] = callBac(testObject[index]);
    }
    return testObject;
  } else {
    return [];
  }
}

module.exports = mapObject;
