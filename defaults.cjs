// const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
function defaults(testObject, defaultProps) {
  if (testObject && defaultProps) {
    for (const index in defaultProps) {
      if (testObject[index] === defaultProps[index]) {
        continue;
      } else {
        testObject[index] = defaultProps[index];
      }
    }
    return testObject;
  } else {
    return [];
  }
}

module.exports = defaults;
