// const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
// console.log("hi");
function invert(testObject) {
  if (testObject) {
    let result = {};
    let swap;
    for (let index in testObject) {
      swap = testObject[index];
      result[swap] = index;
    }
    return result;
  } else {
    return [];
  }
}

module.exports = invert;
