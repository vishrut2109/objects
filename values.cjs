function values(testObject) {
  if (testObject) {
    let result = [];
    for (const index in testObject) {
      result[result.length] = testObject[index];
    }
    return result;
  } else {
    return [];
  }
}

module.exports = values;
