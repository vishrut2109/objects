function keys(testObject) {
  if (testObject) {
    let result = [];
    for (const index in testObject) {
      result[result.length] = index;
    }
    return result;
  } else {
    return [];
  }
}

module.exports = keys;
