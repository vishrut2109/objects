function pairs(testObject) {
  if (testObject) {
    let result = [];
    let subResult = [];
    for (const index in testObject) {
      subResult[subResult.length] = index;
      subResult[subResult.length] = testObject[index];
      result[result.length] = subResult;
      subResult = [];
    }
    return result;
  } else {
    return [];
  }
}

module.exports = pairs;
